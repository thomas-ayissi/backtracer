# BackTracer


## Requires
- [Node JS](https://nodejs.org/en/download/)
 
    - Once installed, you must install globally [Nest](https://nestjs.com/) and [Angular](https://angular.io/) `npm install -g @nestjs/cli @angular/cli`

- [Docker](https://docs.docker.com/get-docker/) 
## Clone the project

Open a terminal in the installation folder and clone the repository
```shell
git clone --recurse-submodules https://gitlab.com/backtracer/backtracer
```

## Project installation 

### From Bash Terminal
Keep your last terminal in the same path and move into the cloned repository. Unzip the database folder and set up submodule's projects : 
```shell
cd backtracer/
unzip ./docker.zip
cd ./API-backtracer/
npm i 
cd ../Front-backtracer/
npm i
```

### With Other Terminal
Now you must unzip the docker.zip file to have this current path in you project : backtracer/docker/db where backtracer is the cloned directory.
Keep your last terminal in the same path and move into the cloned repository. Unzip the database folder and set up submodule's projects :
```shell
cd backtracer/API-backtracer/
npm i 
cd ../Front-backtracer/
npm i
```


## Launch the project (Method 1)
Go back to the root directory of the project `cd ..`

The project is divided in 3 parts 

### **The Postgres Database**
The Postgres Database runs using Docker
Before to launch the database, make sure that Docker is running.
The database has persistent content so you already have to make sur that in the root of the *backtracer* repository you have the following path `docker/db/` and many `pg_` folders inside.

You can now run the database from the `backtracer/` dir
```shell
docker-compose up 
```
This command will launch the database using the persistent content inside `./docker` folder
You also can access to the IHM of the database at [localhost:5431](localhost:5431/)

the database access informations are given in the [.env file](https://gitlab.com/backtracer/backtracer/-/blob/main/.env)

### **The Front Composant**
The Front part of the application can be run like others node project. Make sure to have already execute a npm install inside the `Front-backtracer` folder
```
npm run start
```

### **The API Composant**
The API part of the application can be run like others node project. Make sure to have already execute a npm install inside the `API-backtracer` folder
```
npm run start
```

## Launch the project (Method 2 : Windows Contributors)
For windows contributors, you can launch the file ``launch-processes.bat``

Ouvrir [localhost:4200](https://localhost:4200) pour accéder à l'application web.
